﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterLib;

namespace UnitTestProject1
{
    [TestClass]

    public class UnitTest1
    {
        [TestMethod]

        public void getTotalTest()
        {
            Compteur cpt = new Compteur();

            Assert.AreEqual(0, cpt.getTotal());     // on vérifie que getTotal renvoie bien le total
        }

        [TestMethod]

        public void addTest()
        {
            Compteur cpt = new Compteur();
            cpt.add();                              // on ajoute 1

            Assert.AreEqual(1, cpt.getTotal());     // on vérifie que le compteur vaut 1
        }

        [TestMethod]

        public void removeTest()
        {
            Compteur cpt = new Compteur();
            cpt.add();                              // on ajoute 1 à deux reprises
            cpt.add();
            cpt.remove();                           // on retire 1

            Assert.AreEqual(1, cpt.getTotal());     // on vérifie que le compteur vaut 1
        }

        [TestMethod]

        public void resetTest()
        {
            Compteur cpt = new Compteur();
            cpt.add();                              // on ajoute 1
            cpt.reset();                            // on reset le compteur

            Assert.AreEqual(0, cpt.getTotal());     // on vérifie que le compteur vaut 0
        }

        [TestMethod]

        public void setTotalTest()
        {
            Compteur cpt = new Compteur();

            cpt.setTotal(5);                        // on modifie la valeur du compteur

            Assert.AreEqual(5, cpt.getTotal());     // on vérifie que la valeur a bien été modifiée
        }
    }
}
