﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BasicCounterLib;

namespace BasicCounter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Compteur cpt = new Compteur();

        private void btmoins_Click(object sender, EventArgs e)      // bouton moins
        {
            cpt.remove();
            compteur.Text = string.Format("{0}", cpt.getTotal());
        }

        private void btplus_Click(object sender, EventArgs e)       // bouton plus
        {
            cpt.add();
            compteur.Text = string.Format("{0}", cpt.getTotal());
        }

        private void btRaz_Click(object sender, EventArgs e)        // bouton reset (RAZ)
        {
            cpt.reset();
            compteur.Text = string.Format("{0}", cpt.getTotal());
        }
    }
}
