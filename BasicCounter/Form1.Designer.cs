﻿namespace BasicCounter
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.compteur = new System.Windows.Forms.Label();
            this.btmoins = new System.Windows.Forms.Button();
            this.btplus = new System.Windows.Forms.Button();
            this.btRaz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Linux Biolinum G", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(377, 139);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "total";
            // 
            // compteur
            // 
            this.compteur.AutoSize = true;
            this.compteur.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.compteur.Font = new System.Drawing.Font("Linux Biolinum G", 9.75F, System.Drawing.FontStyle.Bold);
            this.compteur.Location = new System.Drawing.Point(389, 200);
            this.compteur.Name = "compteur";
            this.compteur.Size = new System.Drawing.Size(14, 15);
            this.compteur.TabIndex = 1;
            this.compteur.Text = "0";
            // 
            // btmoins
            // 
            this.btmoins.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmoins.Font = new System.Drawing.Font("Linux Biolinum G", 9.75F, System.Drawing.FontStyle.Bold);
            this.btmoins.Location = new System.Drawing.Point(238, 184);
            this.btmoins.Name = "btmoins";
            this.btmoins.Size = new System.Drawing.Size(75, 50);
            this.btmoins.TabIndex = 2;
            this.btmoins.Text = "-";
            this.btmoins.UseVisualStyleBackColor = true;
            this.btmoins.Click += new System.EventHandler(this.btmoins_Click);
            // 
            // btplus
            // 
            this.btplus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btplus.Font = new System.Drawing.Font("Linux Biolinum G", 9.75F, System.Drawing.FontStyle.Bold);
            this.btplus.Location = new System.Drawing.Point(475, 184);
            this.btplus.Name = "btplus";
            this.btplus.Size = new System.Drawing.Size(75, 50);
            this.btplus.TabIndex = 3;
            this.btplus.TabStop = false;
            this.btplus.Text = "+";
            this.btplus.UseVisualStyleBackColor = true;
            this.btplus.Click += new System.EventHandler(this.btplus_Click);
            // 
            // btRaz
            // 
            this.btRaz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btRaz.Font = new System.Drawing.Font("Linux Biolinum G", 9.75F, System.Drawing.FontStyle.Bold);
            this.btRaz.Location = new System.Drawing.Point(356, 246);
            this.btRaz.Name = "btRaz";
            this.btRaz.Size = new System.Drawing.Size(75, 58);
            this.btRaz.TabIndex = 4;
            this.btRaz.Text = "RAZ";
            this.btRaz.UseVisualStyleBackColor = true;
            this.btRaz.Click += new System.EventHandler(this.btRaz_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btRaz);
            this.Controls.Add(this.btplus);
            this.Controls.Add(this.btmoins);
            this.Controls.Add(this.compteur);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label compteur;
        private System.Windows.Forms.Button btmoins;
        private System.Windows.Forms.Button btplus;
        private System.Windows.Forms.Button btRaz;
    }
}

