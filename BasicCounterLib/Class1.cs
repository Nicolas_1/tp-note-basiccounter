﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounterLib
{
    public class Compteur
    {

        private int total;

        public Compteur()           // constructeur
        {
            total = 0;
        }
        public void add()                   // incrémentation du compteur
        {
            this.total = this.total + 1;
        }

        public void remove()                    // décrémentation du compteur
        {
            if (this.getTotal() != 0)           // on limite le compteur aux nombres positifs
            {
                this.total = this.total - 1;
            }

        }

        public void reset()         // reset du compteur
        {
            this.total = 0;
        }

        public int getTotal()       // getter
        {
            int res = this.total;
            return res;
        }

        public void setTotal(int nombre)    // setter
        {
            if (nombre >= 0)
            {
                this.total = nombre;
            }
        }
    }
}
